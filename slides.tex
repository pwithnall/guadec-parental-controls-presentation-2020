\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{hyperref}
\hypersetup{bookmarks=true, colorlinks=false, linkcolor=blue, citecolor=blue, filecolor=blue, pagecolor=blue, urlcolor=blue,
            pdftitle=Parental controls in GNOME,
            pdfauthor=Philip Withnall, pdfsubject=, pdfkeywords=}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows,automata,positioning,er,calc,decorations.pathmorphing,fit}
\usepackage{listings}
\usepackage{subfigure} % can't use subfig because it doesn't support Beamer
\usepackage{fancyvrb}
\usepackage{ulem}
\usepackage{siunitx}
\usepackage[backend=biber]{biblatex}

% Syntax highlighting colours from the Tango palette: http://pbunge.crimson.ch/2008/12/tango-syntax-highlighting/
\definecolor{LightButter}{rgb}{0.98,0.91,0.31}
\definecolor{LightOrange}{rgb}{0.98,0.68,0.24}
\definecolor{LightChocolate}{rgb}{0.91,0.72,0.43}
\definecolor{LightChameleon}{rgb}{0.54,0.88,0.20}
\definecolor{LightSkyBlue}{rgb}{0.45,0.62,0.81}
\definecolor{LightPlum}{rgb}{0.68,0.50,0.66}
\definecolor{LightScarletRed}{rgb}{0.93,0.16,0.16}
\definecolor{Butter}{rgb}{0.93,0.86,0.25}
\definecolor{Orange}{rgb}{0.96,0.47,0.00}
\definecolor{Chocolate}{rgb}{0.75,0.49,0.07}
\definecolor{Chameleon}{rgb}{0.45,0.82,0.09}
\definecolor{SkyBlue}{rgb}{0.20,0.39,0.64}
\definecolor{Plum}{rgb}{0.46,0.31,0.48}
\definecolor{ScarletRed}{rgb}{0.80,0.00,0.00}
\definecolor{DarkButter}{rgb}{0.77,0.62,0.00}
\definecolor{DarkOrange}{rgb}{0.80,0.36,0.00}
\definecolor{DarkChocolate}{rgb}{0.56,0.35,0.01}
\definecolor{DarkChameleon}{rgb}{0.30,0.60,0.02}
\definecolor{DarkSkyBlue}{rgb}{0.12,0.29,0.53}
\definecolor{DarkPlum}{rgb}{0.36,0.21,0.40}
\definecolor{DarkScarletRed}{rgb}{0.64,0.00,0.00}
\definecolor{Aluminium1}{rgb}{0.93,0.93,0.92}
\definecolor{Aluminium2}{rgb}{0.82,0.84,0.81}
\definecolor{Aluminium3}{rgb}{0.73,0.74,0.71}
\definecolor{Aluminium4}{rgb}{0.53,0.54,0.52}
\definecolor{Aluminium5}{rgb}{0.33,0.34,0.32}
\definecolor{Aluminium6}{rgb}{0.18,0.20,0.21}

% Increase the space after the footnotes so that \footfullcite works
\addtobeamertemplate{footline}{\hfill\usebeamertemplate***{navigation symbols}%
    \hspace*{0.1cm}\par\vskip 20pt}{}

\usetheme{guadec}

\AtBeginSection{\frame{\sectionpage}}


% Abstract here: https://events.gnome.org/event/1/contributions/78/
%
% GNOME 3.36 was the first release containing a new parental controls feature
% upstreamed from Endless OS. What are parental controls, and what do they do in
% GNOME at the moment? How can I integrate my app with parental controls? What
% features are planned for the future? This talk will answer those questions.

\title{Parental controls in GNOME}

\author{Philip Withnall\\Endless\\\texttt{philip@tecnocode.co.uk}}
\date{July 23, 2020}

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\note{25 minutes allocated. 20 minutes for talk, 5 minutes for questions.

Hello, this is an introduction to the parental controls feature which has landed
in various GNOME projects over the last year. I aim to cover it from a user
perspective, and from a technical perspective. Then I’ll look at how you
can integrate with parental controls in your app, and finish with future plans.}


\begin{frame}{Motivation}
\begin{itemize}
	\item{User feedback}
	\item{Growth of flathub}
\end{itemize}
\end{frame}

\note{I have been working on Endless OS for a few years, and one of the
consistent pieces of feedback we got from users and integrators was that they
needed support for parental controls --- the ability to limit what non-admin
users can do on some parts of the system, particularly relating to access to and
installation of content (like websites, or new apps).

With the growth of flathub, a wide variety of apps are now available for users
to install, some of which parents might think are not appropriate for their
kids.

We implemented a first version of parental controls in Endless OS to try things
out, and then started looking at upstreaming it at a hackfest in London in
March 2019 (see
\url{https://tecnocode.co.uk/2019/03/20/parental-controls-hackfest/}).}

\note{That was a productive hackfest, and there was interest in the feature from
a variety of people and distros, and also interest in the related feature of
‘digital wellbeing’.

Digital wellbeing is applying limitations to \emph{yourself} on a computer, in
order to keep yourself on track (not wasting time, not staying on the computer
for too long without a break, not working late, etc.)}


\begin{frame}{What do parental controls do?}
\begin{figure}
	\includegraphics[width=0.35\textwidth]{malcontent-control.png}
	\caption{The Parental Controls app}
\end{figure}
\end{frame}

\note{Currently, parental controls support restricting access to specific
applications chosen by the administrator, with ‘web browers’ having their own
special switch. The administrator can choose to prevent the user installing any
additional applications, or can allow them to only install applications which
are suitable for certain age ranges. Restrictions on applications and
installation currently only work with flatpak apps. They can be applied
separately to each non-admin user, and can only be edited with admin
privileges.

Here’s a screenshot of the parental controls app, which is separate from
gnome-control-center (but linked to from it) because it’s likely to support
statistics and other non-settings-like content in future.}


\begin{frame}{What supports parental controls?}
\begin{itemize}
	\item{gnome-shell}
	\item{flatpak}
	\item{gnome-software}
	\item{gnome-control-center}
	\item{gnome-initial-setup}
\end{itemize}
\end{frame}

\note{There are various integration points for parental controls. The code for
parental controls itself -- the settings widgets and library -- is implemented
in the malcontent project.

\begin{description}
	\item[gnome-shell]{uses \texttt{libmalcontent} to prevent disallowed
	                   apps from being launched or shown in search results.}
	\item[flatpak]{uses it to prevent age-inappropriate apps from being
	               listed or installed, and to prevent disallowed apps from
	               being launched.}
	\item[gnome-software]{uses it for the same.}
	\item[gnome-control-center]{uses it to hide disallowed apps from its app
	                            settings panel; and also has a link through
	                            to the parental controls settings.}
	\item[gnome-initial-setup]{has some more in-depth integration which
	                           allows parent and child users to be created
	                           at initial setup time, and allows the initial
	                           restrictions to be set on the child’s
	                           account. Here’s some screenshots.}
\end{description}}


\begin{frame}{Initial setup}
\begin{figure}
	\includegraphics[width=0.6\textwidth]{gis1.png}
	\caption{Choosing to enable parental controls in gnome-initial-setup}
\end{figure}
\end{frame}

\begin{frame}{Initial setup}
\begin{figure}
	\includegraphics[width=0.6\textwidth]{gis2.png}
	\caption{Setting initial parent controls for a non-admin user in gnome-initial-setup}
\end{figure}
\end{frame}

\begin{frame}{Initial setup}
\begin{figure}
	\includegraphics[width=0.6\textwidth]{gis3.png}
	\caption{Setting a parent password in gnome-initial-setup}
\end{figure}
\end{frame}

\note{The flow of setting up parent and child accounts in gnome-initial-setup.
If the ‘Set up parental controls for this user’ checkbox isn’t checked, the new
flow is bypassed and initial setup proceeds as before.}


\begin{frame}{What’s the uptake?}
% Source: dashboard/16
\begin{figure}
	\includegraphics[width=0.8\textwidth]{parental-controls-users.png}
	\caption{Uptake of parental controls since EOS 3.8 was released}
\end{figure}
\end{frame}

\note{Parental controls integration in gnome-initial-setup was first released
in Endless OS 3.8 (start of May 2020), and we included some anonymous opt-in
statistics reporting to measure the uptake.

This only measures users who’ve installed Endless OS since 3.8 was released, as
users who upgraded from a previous OS version won’t go through initial setup
again. So far, it seems consistently about 2\% of new users enable parental
controls during initial setup.}


\begin{frame}{How do parental controls work?}
\begin{figure}
	\includegraphics[width=\columnwidth]{architecture.pdf}
	\caption{Parental controls architecture (restricting installation)}
\end{figure}
\end{frame}

\note{This diagram shows an example of how parental controls are implemented in
flatpak to potentially disallow the user from installing an app which is not
age-appropriate for them.

Say the user wants to install WolfenDoom (a violent game). flatpak passes the
OARS (Open Age Ratings Service) metadata for WolfenDoom to libmalcontent (the
parental controls library). This metadata contains various ratings to describe
the app: whether it contains bloody violence, religious references, sexual
content, etc. Each app (not just games) should have this metadata, otherwise
it’ll be assumed to be maximally violent, sexual, full of gambling, etc.

libmalcontent then queries accountsservice for the user’s parental controls
settings, which indicate what levels of violence, gambling, etc. the user is
allowed to see. It then compares these settings against the app’s OARS
metadata to work out whether installation is allowed.}

\note{In this example, the user is not allowed to install WolfenDoom as it’s too
violent. flatpak then queries polkit to see if the admin wants to override
malcontent and allow installation anyway. This will pop up a polkit
authorisation dialogue. If the user enters the admin password, installation is
allowed, otherwise it’s denied.

Note that this is all implemented within the same user process space. It’s not
real security: a determined user will always be able to find a way around it
(for example, by downloading and running a statically linked version of
WolfenDoom). It should prevent the average child from doing things they’re not
supposed to, though.}


\begin{frame}{How do parental controls work?}
\begin{figure}
	\includegraphics[width=\columnwidth]{architecture-blocklist.pdf}
	\caption{Parental controls architecture (restricting installed apps)}
\end{figure}
\end{frame}

\note{While restricting installation of new apps is implemented using OARS
metadata, restricting access to apps which are already installed uses a
blocklist. This allows the administrator to have age-inappropriate apps
installed system-wide, but restricted for some users. Like all user data for
parental controls, that blocklist is also stored in accountsservice.}


\begin{frame}{How can I integrate my app with parental controls?}
\Large{Add OARS data to your appstream: \url{https://hughsie.github.io/oars/}}
\end{frame}

\note{If you do nothing else, you should spend 5 minutes doing this: add OARS
metadata to your application please! Even if your application contains no
violence, gambling, advertisements or online chat, its metadata needs to say as
much, otherwise we can’t know.

There’s an easy online questionnaire which generates the right metadata for
you, and you then put it into your appdata file.}


\begin{frame}[fragile]
\frametitle{How can I integrate my app with parental controls?}
\begin{lstlisting}[language=XML]
<content_rating type="oars-1.1">
  <content_attribute id="violence-cartoon">moderate</content_attribute>
  <content_attribute id="violence-fantasy">moderate</content_attribute>
  <content_attribute id="social-chat">intense</content_attribute>
  <content_attribute id="social-info">intense</content_attribute>
  <content_attribute id="social-audio">none</content_attribute>
  <content_attribute id="social-location">none</content_attribute>
  <content_attribute id="social-contacts">none</content_attribute>
  <content_attribute id="money-purchasing">intense</content_attribute>
  <content_attribute id="money-gambling">none</content_attribute>
</content_rating>
\end{lstlisting}
\end{frame}

\note{Here’s an example of some OARS metadata. Note that the attributes are not
all about violence --- there are attributes relating to privacy and advertising
as well, for example. There’s a specification on the OARS website.

If any OARS data is provided, then missing attributes are assumed to have value
\texttt{none}. If no OARS data is provided, they’re all assumed to have value
\texttt{intense}.}


\begin{frame}{How can I integrate my app with parental controls?}
\Large{Use libmalcontent to implement internal filtering}
\\
\vspace{1em}
\small{Example code: \url{https://gitlab.freedesktop.org/pwithnall/malcontent/-/tree/master/malcontent-client}}
\end{frame}

\note{If your application has more complex parental controls needs -- for
example, it can display various kinds of content dynamically and you need to
restrict some of it -- you might want to use \texttt{libmalcontent} within your
app to check the user’s parental controls settings and internally filter what
they are allowed to see or do.

There’s some example code for that in the malcontent repository, but please also
get in touch with me and I can help out.}


\begin{frame}{How can I enable parental controls in my distro?}
\Large{Compile everything with \texttt{libmalcontent} support}
\\
\vspace{1em}
\Large{Ensure all your apps have OARS metadata, and gnome-software can access it}
\end{frame}

\note{Currently, as far as I know, Endless OS is the only distribution with
parental controls enabled. Enabling it in your distribution should be a matter
of enabling the optional \texttt{libmalcontent} dependency in the projects
listed earlier, and ensuring that all the apps you ship have correct OARS
metadata.

If you allow app installation from sources other than flatpak, you may need to
hook up appstream metadata for your package manager to allow gnome-software to
read the OARS metadata for apps, and hence know what kinds of content they
contain.}


\begin{frame}{What about the future?}
\begin{itemize}
	\item{Digital wellbeing}
	\item{Screen time}
	\item{Time limited sessions}
\end{itemize}
\end{frame}

\note{At the hackfest in March 2019, various other parts of the feature were
discussed, and digital wellbeing was enthusiastically received by people. I’d
like to work on that next.

Screen time, in particular, is a nice and self-contained feature which should be
relatively easy to implement. It would limit the amount of time you can spend on
the computer without a break. If anyone is keen to work on it, I am happy to
mentor and provide support.}


\begin{frame}{Screen time mockups}
\begin{figure}
	\includegraphics[width=0.5\textwidth]{screen-time.png}
	\caption{Screen time mockup (Allan Day)}
\end{figure}
\end{frame}

\note{Here’s a mockup for how screen time functionality could look, thanks to
Allan Day.}


\begin{frame}{Miscellany}
\begin{description}
	\item[Slide source]{\url{https://gitlab.com/pwithnall/guadec-parental-controls-presentation-2020}}
	\item[malcontent project]{\url{https://gitlab.freedesktop.org/pwithnall/malcontent}}
\end{description}


% Creative commons logos from http://blog.hartwork.org/?p=52
\vfill
\begin{center}
	\includegraphics[scale=0.83]{cc_by_30.pdf}\hspace*{0.95ex}\includegraphics[scale=0.83]{cc_sa_30.pdf}\\[1.5ex]
	{\tiny\textit{Creative Commons Attribution-ShareAlike 4.0 International License}}\\[1.5ex]
	\vspace*{-2.5ex}
\end{center}

\vfill
\begin{center}
	\tiny{Beamer theme: \url{https://gitlab.gnome.org/GNOME/presentation-templates/tree/master/GUADEC/2020}}
\end{center}
\end{frame}

\end{document}
